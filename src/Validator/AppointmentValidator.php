<?php
declare(strict_types=1);

namespace App\Validator;

use function checkdate;
use function count;
use function explode;
use function preg_match;

/**
 * Class AppointmentValidator
 * @package App\Validator
 */
class AppointmentValidator
{
    /**
     * Validate POST data
     * @param string $inputMembership
     * @param string $inputDate
     * @param string $inputTime
     * @return array
     */
    public function validateAppointmentForm(
        string $inputMembership,
        string $inputDate,
        string $inputTime
    ): array {
        /**
         * Validate membership
         */
        if (empty($inputMembership)) {
            return [
                'status'    => false,
                'message'   => '¡Error! Debe escribir su número de socio.',
            ];
        }

        /**
         * Validate date
         */
        if (!$this->validateDateSpain($inputDate)) {
            return [
                'status'    => false,
                'message'   => '¡Error! Revise el campo fecha.',
            ];
        }

        /**
         * Validate time
         */
        if (!$this->validateTime($inputTime)) {
            return [
                'status'    => false,
                'message'   => '¡Error! Revise el campo hora.',
            ];
        }

        return [
            'status'    => true,
            'message'   => 'ok',
        ];
    }

    /**
     * @param string $time
     * @return false|int
     */
    private function validateTime(string $time)
    {
        return preg_match('/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/', $time);
    }

    /**
     * Validate date Spain
     * @param $date
     * @return bool
     */
    private function validateDateSpain(string $date): bool
    {
        $values = explode('/', $date);
        if (count($values) === 3 && checkdate((int) $values[1], (int) $values[0], (int) $values[2])) {
            return true;
        }

        return false;
    }
}
