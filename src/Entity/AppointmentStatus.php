<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class AppointmentStatus
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "put"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "delete"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "machineName": "exact",
 *         "name": "partial"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "machineName": "ASC",
 *          "name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="appointment_status")
 */
class AppointmentStatus
{
    /**
     * @var int $id
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="machine_name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $machineName;

    /**
     * @var string
     * @ApiProperty(iri="http://schema.org/name")
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $name;

    /**
     * @var Appointment[]|Collection
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="appointmentStatus", fetch="EXTRA_LAZY")
     */
    protected $appointments;

    /**
     * AppointmentType constructor.
     */
    public function __construct()
    {
        $this->appointments = new ArrayCollection();
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getAppointmentsCount(): int
    {
        return count($this->appointments);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMachineName(): string
    {
        return $this->machineName;
    }

    /**
     * @param string $machineName
     * @return AppointmentStatus
     */
    public function setMachineName(string $machineName): self
    {
        $this->machineName = $machineName;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AppointmentStatus
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Appointment[]|Collection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * @param Appointment[]|Collection $appointments
     * @return AppointmentStatus
     */
    public function setAppointments($appointments): self
    {
        $this->appointments = $appointments;
        return $this;
    }
}
