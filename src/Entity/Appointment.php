<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class Appointment
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"date": "DESC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={"access_control"="is_granted('ROLE_AUTHOR')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "put"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "delete"={"access_control"="is_granted('ROLE_AUTHOR')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "userEmployee.id": "exact",
 *         "userEmployee.email": "exact",
 *         "userMember.id": "exact",
 *         "userMember.email": "exact",
 *         "office.id": "exact",
 *         "office.email": "exact",
 *         "appointmentClassification.id": "exact",
 *         "appointmentClassification.machineName": "exact",
 *         "appointmentType.id": "exact",
 *         "appointmentType.machineName": "exact",
 *         "appointmentStatus.id": "exact",
 *         "appointmentStatus.machineName": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "date": "DESC",
 *          "userEmployee.email": "ASC",
 *          "userMember.email": "ASC",
 *          "office.email": "ASC",
 *          "appointmentClassification.name": "ASC",
 *          "appointmentType.name": "ASC",
 *          "appointmentStatus.name": "ASC",
 *          "createdAt": "DESC",
 *          "updatedAt": "DESC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ApiFilter(DateFilter::class, strategy=DateFilter::EXCLUDE_NULL)
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="appointment")
 */
class Appointment
{
    /**
     * @var int $id
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var DateTime $date
     * @ApiProperty(iri="http://schema.org/Date")
     * @ORM\Column(name="date", type="date")
     * @Assert\Date
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="date"}
     *     }
     * )
     * @Groups({"read","write"})
     */
    protected $date;

    /**
     * @var string $time
     * @ORM\Column(name="time", type="string", length=5)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="time", "example"="10:30"}
     *     }
     * )
     * @Groups({"read","write"})
     */
    protected $time;

    /**
     * @var UserEmployee|null $userEmployee
     * @ORM\ManyToOne(targetEntity="UserEmployee", inversedBy="appointments", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="user_employee_id", referencedColumnName="id", nullable=true, onDelete="NO ACTION")
     * @Groups({"read","write"})
     */
    protected $userEmployee;

    /**
     * @var UserMember $userMember
     * @ORM\ManyToOne(targetEntity="UserMember", inversedBy="appointments", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="user_member_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Assert\NotNull
     * @Groups({"read","write"})
     */
    protected $userMember;

    /**
     * @var Office $office
     * @ORM\ManyToOne(targetEntity="Office", inversedBy="appointments", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Assert\NotNull
     * @Groups({"read","write"})
     */
    protected $office;

    /**
     * @var AppointmentClassification $appointmentClassification
     * @ORM\ManyToOne(targetEntity="AppointmentClassification", inversedBy="appointments", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(
     *     name="appointment_classification_id",
     *     referencedColumnName="id",
     *     nullable=false,
     *     onDelete="NO ACTION"
     * )
     * @Groups({"read","write"})
     */
    protected $appointmentClassification;

    /**
     * @var AppointmentType $appointmentType
     * @ORM\ManyToOne(targetEntity="AppointmentType", inversedBy="appointments", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="appointment_type_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Groups({"read","write"})
     */
    protected $appointmentType;

    /**
     * @var AppointmentStatus $appointmentStatus
     * @ORM\ManyToOne(targetEntity="AppointmentStatus", inversedBy="appointments", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="appointment_status_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Groups({"read","write"})
     */
    protected $appointmentStatus;

    /**
     * @var Notification[]|Collection $notifications
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="appointment", fetch="EXTRA_LAZY")
     */
    protected $notifications;

    /**
     * @var DateTime $createdAt
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="datetime"}
     *     }
     * )
     * @Groups({"read"})
     */
    protected $createdAt;

    /**
     * @var DateTime $updatedAt
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @ORM\Column(name="updated_at", type="datetime")
     * @Assert\DateTime
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="datetime"}
     *     }
     * )
     * @Groups({"read"})
     */
    protected $updatedAt;

    /**
     * Appointment constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new DateTime('now'));
        $this->setUpdatedAt(new DateTime('now'));
        $this->notifications = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new DateTime('now'));

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new DateTime('now'));
        }
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getNotificationsCount(): int
    {
        return count($this->notifications);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Appointment
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return Appointment
     */
    public function setDate(DateTime $date): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @param string $time
     * @return Appointment
     */
    public function setTime(string $time): self
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return UserEmployee|null
     */
    public function getUserEmployee(): ?UserEmployee
    {
        return $this->userEmployee;
    }

    /**
     * @param UserEmployee|null $userEmployee
     * @return Appointment
     */
    public function setUserEmployee(?UserEmployee $userEmployee): self
    {
        $this->userEmployee = $userEmployee;
        return $this;
    }

    /**
     * @return UserMember
     */
    public function getUserMember(): UserMember
    {
        return $this->userMember;
    }

    /**
     * @param UserMember $userMember
     * @return Appointment
     */
    public function setUserMember(UserMember $userMember): self
    {
        $this->userMember = $userMember;
        return $this;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @param Office $office
     * @return Appointment
     */
    public function setOffice(Office $office): self
    {
        $this->office = $office;
        return $this;
    }

    /**
     * @return AppointmentClassification
     */
    public function getAppointmentClassification(): AppointmentClassification
    {
        return $this->appointmentClassification;
    }

    /**
     * @param AppointmentClassification $appointmentClassification
     * @return Appointment
     */
    public function setAppointmentClassification(AppointmentClassification $appointmentClassification): self
    {
        $this->appointmentClassification = $appointmentClassification;
        return $this;
    }

    /**
     * @return AppointmentType
     */
    public function getAppointmentType(): AppointmentType
    {
        return $this->appointmentType;
    }

    /**
     * @param AppointmentType $appointmentType
     * @return Appointment
     */
    public function setAppointmentType(AppointmentType $appointmentType): self
    {
        $this->appointmentType = $appointmentType;
        return $this;
    }

    /**
     * @return AppointmentStatus
     */
    public function getAppointmentStatus(): AppointmentStatus
    {
        return $this->appointmentStatus;
    }

    /**
     * @param AppointmentStatus $appointmentStatus
     * @return Appointment
     */
    public function setAppointmentStatus(AppointmentStatus $appointmentStatus): self
    {
        $this->appointmentStatus = $appointmentStatus;
        return $this;
    }

    /**
     * @return Notification[]|Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param Notification[]|Collection $notifications
     * @return Appointment
     */
    public function setNotifications($notifications): self
    {
        $this->notifications = $notifications;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return Appointment
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return Appointment
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
