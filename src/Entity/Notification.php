<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Notification
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"updatedAt": "DESC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "post"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "put"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "delete"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial",
 *         "userType": "exact",
 *         "status": "exact",
 *         "notificationType.id": "exact",
 *         "notificationType.machineName": "exact",
 *         "appointment.id": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "name": "ASC",
 *          "userType": "ASC",
 *          "status": "ASC",
 *          "notificationType.name": "ASC",
 *          "createdAt": "DESC",
 *          "updatedAt": "DESC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ApiFilter(DateFilter::class, strategy=DateFilter::EXCLUDE_NULL)
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="notification")
 */
class Notification
{
    /**
     * @var int $id
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="user_type", type="string", length=10)
     * @ApiProperty(
     *     attributes={
     *          "swagger_context"={
     *              "type"="string",
     *              "enum"={"member", "specialist", "office"},
     *              "example"="specialist"
     *           }
     *     }
     * )
     * @Groups({"read","write"})
     */
    protected $userType = '';

    /**
     * The field is used to send emails to new notification.
     *
     * Two possible cases:
     *
     * 0 - Pending sending email.
     * 1 - Email send.
     *
     * @var int
     * @ORM\Column(name="status", type="integer")
     * @Groups({"read","write"})
     */
    protected $status = 0;

    /**
     * @var NotificationType
     * @ORM\ManyToOne(targetEntity="NotificationType", inversedBy="notifications", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="notification_type_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Groups({"read","write"})
     */
    protected $notificationType;

    /**
     * @var Appointment
     * @ORM\ManyToOne(targetEntity="Appointment", inversedBy="notifications", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="appointment_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Groups({"read","write"})
     */
    protected $appointment;

    /**
     * @var DateTime
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="datetime"}
     *     }
     * )
     * @Groups({"read"})
     */
    protected $createdAt;

    /**
     * @var DateTime
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @ORM\Column(name="updated_at", type="datetime")
     * @Assert\DateTime
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="datetime"}
     *     }
     * )
     * @Groups({"read"})
     */
    protected $updatedAt;

    /**
     * Notification constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new DateTime('now'));
        $this->setUpdatedAt(new DateTime('now'));
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new DateTime('now'));

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new DateTime('now'));
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserType(): string
    {
        return $this->userType;
    }

    /**
     * @param string $userType
     * @return Notification
     */
    public function setUserType(string $userType): self
    {
        $this->userType = $userType;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Notification
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return NotificationType
     */
    public function getNotificationType(): NotificationType
    {
        return $this->notificationType;
    }

    /**
     * @param NotificationType $notificationType
     * @return Notification
     */
    public function setNotificationType(NotificationType $notificationType): self
    {
        $this->notificationType = $notificationType;
        return $this;
    }

    /**
     * @return Appointment
     */
    public function getAppointment(): Appointment
    {
        return $this->appointment;
    }

    /**
     * @param Appointment $appointment
     * @return Notification
     */
    public function setAppointment(Appointment $appointment): self
    {
        $this->appointment = $appointment;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return Notification
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return Notification
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
