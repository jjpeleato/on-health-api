<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class UserEmployee
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"surname": "ASC", "name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={
 *             "route_name"="api_user_employees_post",
 *             "access_control"="is_granted('ROLE_ADMIN')"
 *         }
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "put"={
 *             "route_name"="api_user_employees_put",
 *             "access_control"="is_granted('ROLE_ADMIN')"
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "email": "exact",
 *         "name": "partial",
 *         "surname": "partial",
 *         "phone": "exact",
 *         "mobile": "exact",
 *         "office.id": "exact",
 *         "office.email": "exact",
 *         "userEmployeeType.id": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "email": "ASC",
 *          "name": "ASC",
 *          "surname": "ASC",
 *          "phone": "ASC",
 *          "mobile": "ASC",
 *          "office.name": "ASC",
 *          "userEmployeeType.name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ApiFilter(DateFilter::class, strategy=DateFilter::EXCLUDE_NULL)
 * @ORM\Entity
 * @ORM\Table(name="user_employee")
 */
class UserEmployee extends User
{
    /**
     * @var UserEmployeeType[]|Collection
     * @ORM\ManyToMany(targetEntity="UserEmployeeType", inversedBy="userEmployees", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="user_employee_user_employee_type",
     *     joinColumns={
     *         @ORM\JoinColumn(name="user_employee_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="user_employee_type_id", referencedColumnName="id")
     *     }
     * )
     * @Groups({"read","write"})
     */
    protected $userEmployeeType;

    /**
     * @var Office|null
     * @ORM\ManyToOne(targetEntity="Office", inversedBy="userEmployees", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id", nullable=true, onDelete="NO ACTION")
     * @Assert\NotNull
     * @Groups({"read","write"})
     */
    protected $office;

    /**
     * @var Appointment[]|Collection
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="userEmployee", fetch="EXTRA_LAZY")
     */
    protected $appointments;

    /**
     * UserEmployee constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->userEmployeeType = new ArrayCollection();
        $this->appointments = new ArrayCollection();
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getAppointmentsCount(): int
    {
        return count($this->appointments);
    }

    /**
     * @return UserEmployeeType[]|Collection
     */
    public function getUserEmployeeType()
    {
        return $this->userEmployeeType;
    }

    /**
     * @param UserEmployeeType[]|Collection $userEmployeeType
     * @return UserEmployee
     */
    public function setUserEmployeeType($userEmployeeType): self
    {
        $this->userEmployeeType = $userEmployeeType;
        return $this;
    }

    /**
     * @param UserEmployeeType $userEmployeeType
     */
    public function addUserEmployeeType(UserEmployeeType $userEmployeeType)
    {
        if ($this->userEmployeeType->contains($userEmployeeType)) {
            return;
        }

        $this->userEmployeeType->add($userEmployeeType);
        $userEmployeeType->addUserEmployees($this);
    }

    /**
     * @param UserEmployeeType $userEmployeeType
     */
    public function removeUserEmployeeType(UserEmployeeType $userEmployeeType)
    {
        if (!$this->userEmployeeType->contains($userEmployeeType)) {
            return;
        }

        $this->userEmployeeType->removeElement($userEmployeeType);
        $userEmployeeType->removeUserEmployees($this);
    }

    /**
     * @return Office|null
     */
    public function getOffice(): ?Office
    {
        return $this->office;
    }

    /**
     * @param Office|null $office
     * @return UserEmployee
     */
    public function setOffice(?Office $office): self
    {
        $this->office = $office;
        return $this;
    }

    /**
     * @return Appointment[]|Collection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * @param Appointment[]|Collection $appointments
     * @return UserEmployee
     */
    public function setAppointments($appointments): self
    {
        $this->appointments = $appointments;
        return $this;
    }
}
