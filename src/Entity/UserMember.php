<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class UserMember
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"surname": "ASC", "name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={
 *             "route_name"="api_users_members_post",
 *             "access_control"="is_granted('ROLE_ADMIN')"
 *         }
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "put"={
 *             "route_name"="api_users_members_put",
 *             "access_control"="is_granted('ROLE_ADMIN')"
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "email": "exact",
 *         "name": "partial",
 *         "surname": "partial",
 *         "phone": "exact",
 *         "mobile": "exact",
 *         "office.id": "exact",
 *         "office.email": "exact",
 *         "membership": "exact",
 *         "nif": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "email": "ASC",
 *          "name": "ASC",
 *          "surname": "ASC",
 *          "phone": "ASC",
 *          "mobile": "ASC",
 *          "office.name": "ASC",
 *          "userEmployeeType.name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ApiFilter(DateFilter::class, strategy=DateFilter::EXCLUDE_NULL)
 * @ORM\Entity
 * @ORM\Table(name="user_member")
 */
class UserMember extends User
{
    /**
     * @var string
     * @ORM\Column(name="membership", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $membership;

    /**
     * @var string|null
     * @ORM\Column(name="nif", type="string", length=50, nullable=true)
     * @Groups({"read","write"})
     */
    protected $nif;

    /**
     * @var Office
     * @ORM\ManyToOne(targetEntity="Office", inversedBy="userMembers", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Assert\NotNull
     * @Groups({"read","write"})
     */
    protected $office;

    /**
     * @var Appointment[]|Collection
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="userMember", fetch="EXTRA_LAZY")
     */
    protected $appointments;

    /**
     * UserMember constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->appointments = new ArrayCollection();
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getAppointmentsCount(): int
    {
        return count($this->appointments);
    }

    /**
     * @return string
     */
    public function getMembership(): string
    {
        return $this->membership;
    }

    /**
     * @param string $membership
     * @return UserMember
     */
    public function setMembership(string $membership): self
    {
        $this->membership = $membership;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNif(): ?string
    {
        return $this->nif;
    }

    /**
     * @param string|null $nif
     * @return UserMember
     */
    public function setNif(?string $nif): self
    {
        $this->nif = $nif;
        return $this;
    }

    /**
     * @return Office
     */
    public function getOffice(): Office
    {
        return $this->office;
    }

    /**
     * @param Office $office
     * @return UserMember
     */
    public function setOffice(Office $office): self
    {
        $this->office = $office;
        return $this;
    }

    /**
     * @return Appointment[]|Collection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * @param Appointment[]|Collection $appointments
     * @return UserMember
     */
    public function setAppointments($appointments): self
    {
        $this->appointments = $appointments;
        return $this;
    }
}
