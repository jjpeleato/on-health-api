<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class UserType
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "put"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "delete"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id" :"exact",
 *         "name": "partial"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="user_employee_type")
 */
class UserEmployeeType
{
    /**
     * @var int $id
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="machine_name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $machineName;

    /**
     * @var string
     * @ApiProperty(iri="http://schema.org/name")
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $name;

    /**
     * @var UserEmployee[]|Collection
     * @ORM\ManyToMany(targetEntity="UserEmployee", mappedBy="userEmployeeType", fetch="EXTRA_LAZY")
     */
    protected $userEmployees;

    /**
     * UserEmployeeType constructor.
     */
    public function __construct()
    {
        $this->userEmployees = new ArrayCollection();
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getUserEmployeesCount(): int
    {
        return count($this->userEmployees);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UserEmployeeType
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getMachineName(): string
    {
        return $this->machineName;
    }

    /**
     * @param string $machineName
     * @return UserEmployeeType
     */
    public function setMachineName(string $machineName): self
    {
        $this->machineName = $machineName;
        return $this;
    }

    /**
     * @return UserEmployee[]|Collection
     */
    public function getUserEmployees()
    {
        return $this->userEmployees;
    }

    /**
     * @param UserEmployee[]|Collection $userEmployees
     * @return UserEmployeeType
     */
    public function setUserEmployees($userEmployees): self
    {
        $this->userEmployees = $userEmployees;
        return $this;
    }

    /**
     * @param UserEmployee $userEmployees
     */
    public function addUserEmployees(UserEmployee $userEmployees)
    {
        if ($this->userEmployees->contains($userEmployees)) {
            return;
        }

        $this->userEmployees->add($userEmployees);
        $userEmployees->addUserEmployeeType($this);
    }

    /**
     * @param UserEmployee $userEmployees
     */
    public function removeUserEmployees(UserEmployee $userEmployees)
    {
        if (!$this->userEmployees->contains($userEmployees)) {
            return;
        }

        $this->userEmployees->removeElement($userEmployees);
        $userEmployees->removeUserEmployeeType($this);
    }
}
