<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class Province
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "put"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "delete"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial",
 *         "autonomousCommunity.id": "exact",
 *         "autonomousCommunity.name": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "name": "ASC",
 *          "autonomousCommunity.name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="province")
 */
class Province
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     * @ApiProperty(iri="http://schema.org/name")
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $name;

    /**
     * @var AutonomousCommunity
     * @ORM\ManyToOne(targetEntity="AutonomousCommunity", inversedBy="provinces", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="autonomous_community_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Assert\NotNull
     * @Groups({"read","write"})
     */
    protected $autonomousCommunity;

    /**
     * @var Locality[]|Collection
     * @ORM\OneToMany(targetEntity="Locality", mappedBy="province", fetch="EXTRA_LAZY")
     * @Groups({"read"})
     */
    protected $localities;

    /**
     * Province constructor.
     */
    public function __construct()
    {
        $this->localities = new ArrayCollection();
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getLocalitiesCount(): int
    {
        return count($this->localities);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Province
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return AutonomousCommunity
     */
    public function getAutonomousCommunity(): AutonomousCommunity
    {
        return $this->autonomousCommunity;
    }

    /**
     * @param AutonomousCommunity $autonomousCommunity
     * @return Province
     */
    public function setAutonomousCommunity(AutonomousCommunity $autonomousCommunity): self
    {
        $this->autonomousCommunity = $autonomousCommunity;
        return $this;
    }

    /**
     * @return Locality[]|Collection
     */
    public function getLocalities()
    {
        return $this->localities;
    }

    /**
     * @param Locality[]|Collection $localities
     * @return Province
     */
    public function setLocalities($localities): self
    {
        $this->localities = $localities;
        return $this;
    }
}
