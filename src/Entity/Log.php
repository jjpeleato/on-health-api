<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Log
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"timestamp": "DESC"},
 *         "normalization_context"={"groups"={"read"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "post"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "put"={"access_control"="is_granted('ROLE_ADMIN')"},
 *         "delete"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial",
 *         "email": "partial",
 *         "ip": "exact",
 *         "type": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "name": "ASC",
 *          "email": "ASC",
 *          "ip": "ASC",
 *          "timestamp": "DESC",
 *          "type": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ApiFilter(DateFilter::class, strategy=DateFilter::EXCLUDE_NULL)
 * @ORM\Entity
 * @ORM\Table(name="log")
 */
class Log
{
    /**
     * @var int $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string|null
     * @ApiProperty(iri="http://schema.org/name")
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     * @Groups({"read","write"})
     */
    protected $name;

    /**
     * @var string|null
     * @ApiProperty(iri="http://schema.org/email")
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     * @Groups({"read","write"})
     */
    protected $email;

    /**
     * @var string|null
     * @ORM\Column(name="ip", type="string", length=16, nullable=true)
     * @Groups({"read","write"})
     */
    protected $ip;

    /**
     * @var DateTime
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @ORM\Column(name="timestamp", type="datetime")
     * @Assert\DateTime
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string", "format"="date-time"}
     *     }
     * )
     * @Groups({"read","write"})
     */
    protected $timestamp;


    /**
     * @var string $type
     * @ORM\Column(name="type", type="string", length=10)
     * @ApiProperty(
     *     attributes={
     *          "swagger_context"={"type"="string", "enum"={"access", "error"}, "example"="access"}
     *     }
     * )
     * @Groups({"read","write"})
     */
    protected $type;

    /**
     * Log constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->setTimestamp(new DateTime('now'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Log
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Log
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string|null $ip
     * @return Log
     */
    public function setIp(?string $ip): self
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTimestamp(): DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param DateTime $timestamp
     * @return Log
     */
    public function setTimestamp(DateTime $timestamp): self
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Log
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }
}
