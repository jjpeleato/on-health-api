<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class Locality
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "put"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "delete"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial",
 *         "province.id": "exact",
 *         "province.name": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "name": "ASC",
 *          "province.name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="locality")
 */
class Locality
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     * @ApiProperty(iri="http://schema.org/name")
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $name;

    /**
     * @var Province
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="localities", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Assert\NotNull
     * @Groups({"read","write"})
     */
    protected $province;

    /**
     * @var Office[]|Collection
     * @ORM\OneToMany(targetEntity="Office", mappedBy="locality", fetch="EXTRA_LAZY")
     * @Groups({"read"})
     */
    protected $offices;

    /**
     * Locality constructor.
     */
    public function __construct()
    {
        $this->offices = new ArrayCollection();
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getOfficesCount(): int
    {
        return count($this->offices);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Locality
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Province
     */
    public function getProvince(): Province
    {
        return $this->province;
    }

    /**
     * @param Province $province
     * @return Locality
     */
    public function setProvince(Province $province): self
    {
        $this->province = $province;
        return $this;
    }

    /**
     * @return Office[]|Collection
     */
    public function getOffices()
    {
        return $this->offices;
    }

    /**
     * @param Office[]|Collection $offices
     * @return Locality
     */
    public function setOffices($offices): self
    {
        $this->offices = $offices;
        return $this;
    }
}
