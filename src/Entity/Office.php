<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class Office
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={"access_control"="is_granted('ROLE_EDITOR')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_EDITOR')"},
 *         "put"={"access_control"="is_granted('ROLE_EDITOR')"},
 *         "delete"={"access_control"="is_granted('ROLE_EDITOR')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial",
 *         "email": "exact",
 *         "phone": "exact",
 *         "fax": "exact",
 *         "address": "partial",
 *         "locality.id": "exact",
 *         "locality.name": "exact"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "name": "ASC",
 *          "email": "ASC",
 *          "phone": "ASC",
 *          "fax": "ASC",
 *          "address": "ASC",
 *          "locality.name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="office")
 */
class Office
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     * @ApiProperty(iri="http://schema.org/name")
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $name;

    /**
     * @var string
     * @ApiProperty(iri="http://schema.org/email")
     * @ORM\Column(name="email", type="string", length=50)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=50)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $phone;

    /**
     * @var string|null
     * @ORM\Column(name="fax", type="string", length=50, nullable=true)
     * @Groups({"read","write"})
     */
    protected $fax;

    /**
     * @var string|null
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Groups({"read","write"})
     */
    protected $address;

    /**
     * @var string|null
     * @ORM\Column(name="latitude", type="string", length=50, nullable=true)
     * @Groups({"read","write"})
     */
    protected $latitude;

    /**
     * @var string|null
     * @ORM\Column(name="longitude", type="string", length=50, nullable=true)
     * @Groups({"read","write"})
     */
    protected $longitude;

    /**
     * @var Locality
     * @ORM\ManyToOne(targetEntity="Locality", inversedBy="offices", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="locality_id", referencedColumnName="id", nullable=false, onDelete="NO ACTION")
     * @Assert\NotNull
     * @Groups({"read","write"})
     */
    protected $locality;

    /**
     * @var Appointment[]|Collection
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="office", fetch="EXTRA_LAZY")
     */
    protected $appointments;

    /**
     * @var UserEmployee[]|Collection
     * @ORM\OneToMany(targetEntity="UserEmployee", mappedBy="office", fetch="EXTRA_LAZY")
     */
    protected $userEmployees;

    /**
     * @var UserMember[]|Collection
     * @ORM\OneToMany(targetEntity="UserMember", mappedBy="office", fetch="EXTRA_LAZY")
     */
    protected $userMembers;

    /**
     * Office constructor.
     */
    public function __construct()
    {
        $this->appointments = new ArrayCollection();
        $this->userEmployees = new ArrayCollection();
        $this->userMembers = new ArrayCollection();
    }

    /**
     * @param string $machineName
     * @return int
     */
    public function getUserEmployeesByTypeCount(string $machineName): int
    {
        $count = 0;
        foreach ($this->userEmployees as $i => $userEmployee) {
            $values = $userEmployee->getUserEmployeeType();

            foreach ($values as $j => $value) {
                if ($value->getMachineName() === $machineName) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getUserEmployeesCount(): int
    {
        return count($this->userEmployees);
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getUserMembersCount(): int
    {
        return count($this->userMembers);
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getAppointmentsCount(): int
    {
        return count($this->appointments);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Locality
     */
    public function getLocality(): Locality
    {
        return $this->locality;
    }

    /**
     * @param Locality $locality
     * @return Office
     */
    public function setLocality(Locality $locality): self
    {
        $this->locality = $locality;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Office
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Office
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Office
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }

    /**
     * @param string|null $fax
     * @return Office
     */
    public function setFax(?string $fax): self
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return Office
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    /**
     * @param string|null $latitude
     * @return Office
     */
    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    /**
     * @param string|null $longitude
     * @return Office
     */
    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return UserEmployee[]|Collection
     */
    public function getUserEmployees()
    {
        return $this->userEmployees;
    }

    /**
     * @param UserEmployee[]|Collection $userEmployees
     * @return Office
     */
    public function setUserEmployees($userEmployees): self
    {
        $this->userEmployees = $userEmployees;
        return $this;
    }

    /**
     * @return UserMember[]|Collection
     */
    public function getUsersMembers()
    {
        return $this->userMembers;
    }

    /**
     * @param UserMember[]|Collection $userMembers
     * @return Office
     */
    public function setUserMembers($userMembers): self
    {
        $this->userMembers = $userMembers;
        return $this;
    }

    /**
     * @return Appointment[]|Collection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * @param Appointment[]|Collection $appointments
     * @return Office
     */
    public function setAppointments($appointments): self
    {
        $this->appointments = $appointments;
        return $this;
    }
}
