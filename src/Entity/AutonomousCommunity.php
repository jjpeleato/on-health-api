<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use function count;

/**
 * Class AutonomousCommunity
 * @package App\Entity
 *
 * @ApiResource(
 *     attributes={
 *         "order"={"name": "ASC"},
 *         "normalization_context"={"groups"={"read"}},
 *         "denormalization_context"={"groups"={"write"}}
 *     },
 *     collectionOperations={
 *         "get"={"access_control"="is_granted('ROLE_AUTHOR')"},
 *         "post"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "put"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "delete"={"access_control"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *         "id": "exact",
 *         "name": "partial"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *          "id": "ASC",
 *          "name": "ASC"
 *     },
 *     arguments={
 *         "orderParameterName"="order"
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="autonomous_community")
 */
class AutonomousCommunity
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     * @ApiProperty(iri="http://schema.org/name")
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     * @Assert\NotBlank
     * @Groups({"read","write"})
     */
    protected $name;

    /**
     * @var Province[]|Collection
     * @ORM\OneToMany(targetEntity="Province", mappedBy="autonomousCommunity", fetch="EXTRA_LAZY")
     * @Groups({"read"})
     */
    protected $provinces;

    /**
     * AutonomousCommunity constructor.
     */
    public function __construct()
    {
        $this->provinces = new ArrayCollection();
    }

    /**
     * @Groups({"read"})
     * @return int
     */
    public function getProvincesCount(): int
    {
        return count($this->provinces);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AutonomousCommunity
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Province[]|Collection
     */
    public function getProvinces()
    {
        return $this->provinces;
    }

    /**
     * @param Province[]|Collection $provinces
     *
     * @return AutonomousCommunity
     */
    public function setProvinces($provinces): self
    {
        $this->provinces = $provinces;
        return $this;
    }
}
