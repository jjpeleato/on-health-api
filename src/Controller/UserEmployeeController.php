<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\UserEmployee;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserEmployeeController
 * @package App\Controller
 * @Route("/api")
 */
class UserEmployeeController
{
    /**
     * @Route(
     *     name="api_user_employees_post",
     *     path="/user_employees",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=UserEmployee::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     * @param UserEmployee $data
     * @param UserPasswordEncoderInterface $encoder
     * @return UserEmployee
     */
    public function postAction(UserEmployee $data, UserPasswordEncoderInterface $encoder): UserEmployee
    {
        return $this->encodePassword($data, $encoder);
    }

    /**
     * @Route(
     *     name="api_user_employees_put",
     *     path="/user_employees/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=UserEmployee::class,
     *         "_api_item_operation_name"="put"
     *     }
     * )
     * @param UserEmployee $data
     * @param UserPasswordEncoderInterface $encoder
     * @return UserEmployee
     */
    public function putAction(UserEmployee $data, UserPasswordEncoderInterface $encoder): UserEmployee
    {
        return $this->encodePassword($data, $encoder);
    }

    /**
     * @param UserEmployee $data
     * @param UserPasswordEncoderInterface $encoder
     * @return UserEmployee
     */
    protected function encodePassword(UserEmployee $data, UserPasswordEncoderInterface $encoder): UserEmployee
    {
        $encoded = $encoder->encodePassword($data, $data->getPassword());
        $data->setPassword($encoded);

        return $data;
    }
}
