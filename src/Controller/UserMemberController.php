<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\UserMember;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserMemberController
 * @package App\Controller
 * @Route("/api")
 */
class UserMemberController
{
    /**
     * @Route(
     *     name="api_users_members_post",
     *     path="/user_members",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=UserMember::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     * @param UserMember $data
     * @param UserPasswordEncoderInterface $encoder
     * @return UserMember
     */
    public function postAction(UserMember $data, UserPasswordEncoderInterface $encoder): UserMember
    {
        return $this->encodePassword($data, $encoder);
    }

    /**
     * @Route(
     *     name="api_users_members_put",
     *     path="/user_members/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=UserMember::class,
     *         "_api_item_operation_name"="put"
     *     }
     * )
     * @param UserMember $data
     * @param UserPasswordEncoderInterface $encoder
     * @return UserMember
     */
    public function putAction(UserMember $data, UserPasswordEncoderInterface $encoder): UserMember
    {
        return $this->encodePassword($data, $encoder);
    }

    /**
     * @param UserMember $data
     * @param UserPasswordEncoderInterface $encoder
     * @return UserMember
     */
    protected function encodePassword(UserMember $data, UserPasswordEncoderInterface $encoder): UserMember
    {
        $encoded = $encoder->encodePassword($data, $data->getPassword());
        $data->setPassword($encoded);

        return $data;
    }
}
