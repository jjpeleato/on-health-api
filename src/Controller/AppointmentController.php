<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Appointment;
use App\Entity\Office;
use App\Entity\UserMember;
use App\Service\AppointmentService;
use App\Service\NotificationService;
use App\Validator\AppointmentValidator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

use function set_time_limit;

/**
 * Class AppointmentController
 * @package App\Controller
 */
class AppointmentController extends AbstractController
{
    /** @var AppointmentService $appointmentService */
    protected $appointmentService;

    /** @var NotificationService $notificationService */
    protected $notificationService;

    /** @var AppointmentValidator $appointmentValidator */
    protected $appointmentValidator;

    /**
     * AppointmentController constructor.
     * @param AppointmentService $appointmentService
     * @param NotificationService $notificationService
     */
    public function __construct(AppointmentService $appointmentService, NotificationService $notificationService)
    {
        $this->appointmentService = $appointmentService;
        $this->notificationService = $notificationService;
        $this->appointmentValidator = new AppointmentValidator();
    }

    /**
     * @return JsonResponse
     */
    public function appointmentAction()
    {
        set_time_limit(0); // 0 = no limits

        /**
         * Security AJAX request
         */
        $post = isset($_POST) ? $_POST : null;
        if (!isset($post)) {
            return new JsonResponse([
                'status'    => 405,
                'code'      => 'E1',
                'message'   => '¡Ups! Algo no ha ido bien.',
            ]);
        }

        /**
         * Validate if the input key match with APP_KEY
         */
        $inputKey = !empty($post['inputKey']) ? $post['inputKey'] : '';
        $parameterHash = $this->getParameter('APP_KEY');
        if ($inputKey !== $parameterHash) {
            return new JsonResponse([
                'status'    => 401,
                'code'      => 'E2',
                'message'   => 'Usted no está autorizado.',
            ]);
        }

        /**
         * Validate the POST data
         */
        $inputMembership = !empty($post['inputMembership']) ? $post['inputMembership'] : '';
        $inputDate = !empty($post['inputDate']) ? $post['inputDate'] : '';
        $inputTime = !empty($post['inputTime']) ? $post['inputTime'] : '';
        $validatorForm = $this->appointmentValidator->validateAppointmentForm(
            $inputMembership,
            $inputDate,
            $inputTime
        );
        if (!$validatorForm['status']) {
            return new JsonResponse([
                'status'    => 400,
                'code'      => 'E3',
                'message'   => $validatorForm['message'],
            ]);
        }

        /**
         * Get user according membership
         * @var UserMember[] $userMember
         */
        $userMember = $this->appointmentService->getUserAccordingMembership($inputMembership, 1);
        if (empty($userMember)) {
            return new JsonResponse([
                'status'    => 400,
                'code'      => 'E4',
                'message'   => 'El nº de socio que ha introducido no existe, póngase en contacto con su oficina.',
            ]);
        }

        /**
         * Validate if there are specialists according to office
         * @var Office $office
         */
        $office = $userMember[0]->getOffice();
        $officeId = $office->getId();
        $officeName = $office->getName();
        $numberOfSpecialist = $office->getUserEmployeesByTypeCount('specialist');
        if ($numberOfSpecialist === 0) {
            return new JsonResponse([
                'status'    => 400,
                'code'      => 'E5',
                'message'   => '¡Vaya! La oficina ' . $officeName
                    . ' no tiene actualmente especialistas para atenderle.',
            ]);
        }

        /**
         * Validate if the date and time is not reserved
         */
        try {
            $isFreeDateAndTime = $this->appointmentService->isFreeDateAndTimeAccordingOffice(
                $inputDate,
                $inputTime,
                $officeId,
                $numberOfSpecialist
            );
        } catch (Exception $e) {
            return new JsonResponse([
                'status'    => 500,
                'code'      => 'E6',
                'message'   => '¡Ups! Algo no ha ido bien.',
                'error'     => $e->getMessage(),
                'trace'     => $e->getTraceAsString(),
            ]);
        }
        if (!$isFreeDateAndTime) {
            return new JsonResponse([
                'status'    => 400,
                'code'      => 'E7',
                'message'   => '¡Vaya! La oficina ' . $officeName
                    . ' no tiene disponibilidad. Pruebe otro día u hora.',
            ]);
        }

        /**
         * Create new appoitment according data
         */
        try {
            /**
             * @var Appointment $appointment
             */
            $appointment = $this->appointmentService->saveAppointment(
                $userMember[0],
                $office,
                $inputDate,
                $inputTime
            );
            $idAppointment = $appointment->getId();
        } catch (OptimisticLockException $e) {
            return new JsonResponse([
                'status'    => 500,
                'code'      => 'E8',
                'message'   => '¡Ups! Algo no ha ido bien.',
                'error'     => $e->getMessage(),
                'trace'     => $e->getTraceAsString(),
            ]);
        } catch (ORMException $e) {
            return new JsonResponse([
                'status'    => 500,
                'code'      => 'E9',
                'message'   => '¡Ups! Algo no ha ido bien.',
                'error'     => $e->getMessage(),
                'trace'     => $e->getTraceAsString(),
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'status'    => 500,
                'code'      => 'E10',
                'message'   => '¡Ups! Algo no ha ido bien.',
                'error'     => $e->getMessage(),
                'trace'     => $e->getTraceAsString(),
            ]);
        }

        /**
         * Create the notifications triggers for send emails according cron job
         */
        try {
            $this->notificationService->saveNotificationTriggers($appointment, 'nt_dm', 'member');
            $this->notificationService->saveNotificationTriggers($appointment, 'nt_dm', 'office');
        } catch (OptimisticLockException $e) {
            return new JsonResponse([
                'status'    => 500,
                'code'      => 'E11',
                'message'   => '¡Ups! Algo no ha ido bien.',
                'error'     => $e->getMessage(),
                'trace'     => $e->getTraceAsString(),
            ]);
        } catch (ORMException $e) {
            return new JsonResponse([
                'status'    => 500,
                'code'      => 'E12',
                'message'   => '¡Ups! Algo no ha ido bien.',
                'error'     => $e->getMessage(),
                'trace'     => $e->getTraceAsString(),
            ]);
        }

        /**
         * Process run correctly
         */
        return new JsonResponse([
            'status'    => 200,
            'code'      => 'A1',
            'message'   => '¡Genial! Nº cita: "<strong>'
                . $idAppointment . '</strong>". Su petición ha sido registrada y quedá en estado pendiente de validar.',
        ]);
    }
}
