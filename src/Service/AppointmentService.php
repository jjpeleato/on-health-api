<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Appointment;
use App\Entity\AppointmentClassification;
use App\Entity\AppointmentStatus;
use App\Entity\AppointmentType;
use App\Entity\Office;
use App\Entity\UserMember;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException as ORMExceptionAlias;
use Exception;

use function count;

class AppointmentService
{
    /** @var EntityManager */
    private $em;

    /**
     * AppointmentService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get user according membership.
     *
     * @param string $membership
     * @param int $limit
     * @return array
     */
    public function getUserAccordingMembership(string $membership, int $limit): array
    {
        $userMember = $this->em
            ->getRepository(UserMember::class)
            ->findBy(['membership' => $membership], null, $limit);

        return $userMember;
    }

    /**
     * Validate that the date and time are free based on the maximum number of employees in that office.
     *
     * @param string $date
     * @param string $time
     * @param int $officeId
     * @param int $limit
     * @return bool
     * @throws Exception
     */
    public function isFreeDateAndTimeAccordingOffice(string $date, string $time, int $officeId, int $limit): bool
    {
        $dateSql = DateTime::createFromFormat('d/m/Y', $date);
        $dateSqlFormat = $dateSql->format('Y-m-d');

        $appointments = $this->em
            ->getRepository(Appointment::class)
            ->findBy([
                'date' => new DateTime($dateSqlFormat),
                'time' => $time,
                'office' => $officeId,
            ]);

        if (count($appointments) < $limit) {
            return true;
        }

        return false;
    }

    /**
     * Save appoitment according data
     *
     * @param UserMember $userMember
     * @param Office $office
     * @param string $inputDate
     * @param string $inputTime
     * @return int
     * @throws ORMExceptionAlias
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function saveAppointment(
        UserMember $userMember,
        Office $office,
        string $inputDate,
        string $inputTime
    ): Appointment {
        /**
         * @var AppointmentClassification $appointmentClassification
         */
        $appointmentClassification = $this->em
            ->getRepository(AppointmentClassification::class)
            ->findOneBy(['machineName' => 'ac_normal']);
        /**
         * @var AppointmentStatus $appointmentStatus
         */
        $appointmentStatus = $this->em
            ->getRepository(AppointmentStatus::class)
            ->findOneBy(['machineName' => 'as_check']);
        /**
         * @var AppointmentType $appointmentType
         */
        $appointmentType = $this->em
            ->getRepository(AppointmentType::class)
            ->findOneBy(['machineName' => 'at_session']);

        /**
         * SQL date and time
         */
        $dateSql = DateTime::createFromFormat('d/m/Y', $inputDate);
        $dateSqlFormat = $dateSql->format('Y-m-d');

        /**
         * New appointment
         */
        $appointment = new Appointment();
        $appointment->setUserMember($userMember)
            ->setOffice($office)
            ->setAppointmentClassification($appointmentClassification)
            ->setAppointmentStatus($appointmentStatus)
            ->setAppointmentType($appointmentType)
            ->setDate(new DateTime($dateSqlFormat))
            ->setTime($inputTime);

        $this->em->persist($appointment);
        $this->em->flush();

        return $appointment;
    }
}
