<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Appointment;
use App\Entity\Notification;
use App\Entity\NotificationType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException as ORMExceptionAlias;

class NotificationService
{
    /** @var EntityManager */
    private $em;

    /**
     * NotificationService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Save notification triggers according appointment, machineName and userType
     *
     * @param Appointment $appointment
     * @param string $machineName
     * @param string $userType
     * @return Notification
     * @throws ORMExceptionAlias
     * @throws OptimisticLockException
     */
    public function saveNotificationTriggers(
        Appointment $appointment,
        string $machineName,
        string $userType
    ): Notification {
        /**
         * @var NotificationType $notificationType
         */
        $notificationType = $this->em
            ->getRepository(NotificationType::class)
            ->findOneBy(['machineName' => $machineName]);

        $notification = new Notification();
        $notification->setUserType($userType)
            ->setStatus(0)
            ->setAppointment($appointment)
            ->setNotificationType($notificationType);

        $this->em->persist($notification);
        $this->em->flush();

        return $notification;
    }
}
