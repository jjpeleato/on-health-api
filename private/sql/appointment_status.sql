INSERT INTO `appointment_status` (`id`, `machine_name`, `name`) VALUES
(NULL, 'as_accept', 'Aceptada'),
(NULL, 'as_cancel', 'Cancelada'),
(NULL, 'as_check', 'Pendiente de revisión'),
(NULL, 'as_schedule', 'Pendiente de programar');
