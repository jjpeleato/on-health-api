INSERT INTO `office` (`id`, `locality_id`, `name`, `email`, `phone`, `fax`, `address`, `latitude`, `longitude`) VALUES
(NULL, '9', 'Asociación Huesca', 'huesca@aragon.com', '974974974', '', 'Plaza de la Catedral, 1, 22002', NULL, NULL),
(NULL, '10', 'Asociación Teruel', 'teruel@aragon.com', '978978978', '', 'Plaza de la Catedral, 1, 44001', NULL, NULL),
(NULL, '11', 'Asociación Zaragoza', 'zaragoza@aragon.com', '976721100', '', 'Plaza Ntra. Sra. del Pilar, 18, 50003', NULL, NULL);
