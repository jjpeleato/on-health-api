INSERT INTO `user_employee_type` (`id`, `machine_name`, `name`) VALUES
(NULL, 'administration', 'Administración'),
(NULL, 'specialist', 'Especialista'),
(NULL, 'practices', 'Prácticas'),
(NULL, 'secretary', 'Secretaría'),
(NULL, 'social_worker', 'Trabajador social');
