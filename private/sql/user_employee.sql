-- Create the some users
-- password: $2y$12$mHn9oSl2vNfB6jwCv1Fh7.5.gbLfKW9k9tX90kpZLJv5WmQGgAksy (1234)
INSERT INTO `user` (`id`, `email`, `roles`, `password`, `name`, `surname`, `birthday`, `phone`, `mobile`, `created_at`, `updated_at`, `discr`) VALUES
('047dc80c-1098-4f93-9b5c-9a33a2b9eb67', 'yluh1hh016@yahoo.es', 'a:1:{i:0;s:11:"ROLE_AUTHOR";}', '$2y$12$mHn9oSl2vNfB6jwCv1Fh7.5.gbLfKW9k9tX90kpZLJv5WmQGgAksy', 'Alvaro', 'Lucas Pereira', '1945-01-23', NULL, '662160620', now(), now(), 'userEmployee'),
('c0455169-b761-43d5-b490-b0b52712df12', 'i7lymjjgsd@msn.com', 'a:1:{i:0;s:11:"ROLE_AUTHOR";}', '$2y$12$mHn9oSl2vNfB6jwCv1Fh7.5.gbLfKW9k9tX90kpZLJv5WmQGgAksy', 'Nora', 'Alonso Luis', '1996-04-23', NULL, '798750342', now(), now(), 'userEmployee'),
('e90a7b17-57d2-475c-8a2c-6cafc58788fb', 'a8mhz2yza@lycos.de', 'a:1:{i:0;s:11:"ROLE_EDITOR";}', '$2y$12$mHn9oSl2vNfB6jwCv1Fh7.5.gbLfKW9k9tX90kpZLJv5WmQGgAksy', 'Itziar', 'Rovira Ayala', '1944-03-04', NULL, '723965087', now(), now(), 'userEmployee'),
('0dafbbad-09e1-4f99-915d-14411d1cf42c', '163hxgud@lycos.at', 'a:1:{i:0;s:11:"ROLE_EDITOR";}', '$2y$12$mHn9oSl2vNfB6jwCv1Fh7.5.gbLfKW9k9tX90kpZLJv5WmQGgAksy', 'Mustapha', 'Crespo Ayala', '1959-06-23', NULL, '712460310', now(), now(), 'userEmployee'),
('e5e36d37-4760-4a43-acce-e7fa17a0a85f', 'axfpkps23u@blu.it', 'a:1:{i:0;s:10:"ROLE_ADMIN";}', '$2y$12$mHn9oSl2vNfB6jwCv1Fh7.5.gbLfKW9k9tX90kpZLJv5WmQGgAksy', 'Paulino', 'Angulo Mateu', '1978-10-09', NULL, '622668370', now(), now(), 'userEmployee');

-- Assign office to the users
INSERT INTO `user_employee` (`id`, `office_id`) VALUES
('047dc80c-1098-4f93-9b5c-9a33a2b9eb67', '1'),
('c0455169-b761-43d5-b490-b0b52712df12', '1'),
('e90a7b17-57d2-475c-8a2c-6cafc58788fb', '2'),
('0dafbbad-09e1-4f99-915d-14411d1cf42c', '2'),
('e5e36d37-4760-4a43-acce-e7fa17a0a85f', '3');

-- Assign type to the user
INSERT INTO `user_employee_user_employee_type` (`user_employee_id`, `user_employee_type_id`) VALUES
('047dc80c-1098-4f93-9b5c-9a33a2b9eb67', '2'),
('c0455169-b761-43d5-b490-b0b52712df12', '2'),
('e90a7b17-57d2-475c-8a2c-6cafc58788fb', '4'),
('0dafbbad-09e1-4f99-915d-14411d1cf42c', '5'),
('e5e36d37-4760-4a43-acce-e7fa17a0a85f', '1');
