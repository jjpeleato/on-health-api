(function ($) {
    $(document).ready(function () {
        AOS.init();
        setIE();
        getBackgroundImage();
        setDatepicker();
        setTimepicker();
        submitForm();
    });
})(jQuery);
