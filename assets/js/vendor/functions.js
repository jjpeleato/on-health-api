/**
 * Detect IE. Returns version of IE or false, if browser is not Internet Explorer
 *
 * @returns {boolean|number}
 */
function detectIE() {
    let ua = window.navigator.userAgent;

    let msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    let trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        let rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    let edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

/**
 * Is Internet Explorer
 *
 * @returns {boolean}
 */
function isIE() {
    let version = detectIE();

    if (version === false) {
        return false;
    }

    return true;
}

/**
 * Set IE class into html tag
 */
function setIE() {
    let html = jQuery('html');
    html.removeClass('ie');

    if (isIE()) {
        html.addClass('ie');
    }
}

/**
 * Is mobile
 *
 * @returns {boolean}
 */
function isMobile() {
    let width = jQuery(window).width();
    return (width < 769);
}

/**
 * Remove some special characters from text string and to lower case
 *
 * @param str
 * @returns {*}
 */
function getNormalizeAndToLowerCase(str) {
    if (isIE()) {
        return str;
    }

    return str
        .normalize('NFD')
        .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi, "$1")
        .normalize()
        .toLowerCase();
}

/**
 * Get current scroll
 *
 * @returns {number}
 */
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
}

/**
 * Datepicker system
 */
function setDatepicker() {
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();

    let picker = datepicker('.datepicker', {
        formatter: function(input, date, instance) {
            // This will display the date as `1/1/2019`.
            input.value = date.toLocaleDateString();
        },
        startDay: 1,
        customDays: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'S&aacute;b'],
        customMonths: [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        ],
        overlayPlaceholder: 'Buscar año...',
        overlayButton: "¡Vamos!",
        minDate: new Date(year, month, day),
        startDate: date,
        noWeekends: true
    });
}

/**
 * Timepicker system
 */
function setTimepicker() {
    jQuery('.timepicker').timepicker({
        timeFormat: 'H:mm',
        interval: 30,
        minTime: '9',
        maxTime: '19:30',
        startTime: '09:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
}
