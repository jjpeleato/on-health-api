/**
 * Clean form fields
 */
function cleanForm() {
    jQuery("#inputMembership").val('');
    jQuery("#inputDate").val('');
    jQuery("#inputTime").val('');
    jQuery("#inputPrivacy").attr('checked', false);
}

/**
 * Validate form
 *
 * @param domContentHeader
 * @param domContentFooter
 * @param inputPrivacy
 * @param inputMembership
 * @param inputDate
 * @param inputTime
 * @returns {boolean}
 */
function validateForm(
    domContentHeader,
    domContentFooter,
    inputPrivacy,
    inputMembership,
    inputDate,
    inputTime
) {
    if (inputPrivacy === false) {
        domContentHeader.removeClass('content-header--error').addClass('content-header--error');
        domContentFooter.removeClass('content-footer--error')
            .addClass('content-footer--error')
            .empty()
            .append(
                "<p class='content-footer__text'>¡Error! Debe aceptar las condiciones del aviso legal.</p>"
            );
        return false;
    }


    if (inputMembership === null || inputMembership.length === 0 || /^\s+$/.test(inputMembership)) {
        domContentHeader.removeClass('content-header--error').addClass('content-header--error');
        domContentFooter.removeClass('content-footer--error')
            .addClass('content-footer--error')
            .empty()
            .append(
                "<p class='content-footer__text'>¡Error! Debe escribir su número de socio.</p>"
            );
        return false;
    }


    let dateFormat = ['D/M/YYYY'];
    let validateDate = moment(inputDate,  dateFormat, true).isValid();
    if (validateDate === false) {
        domContentHeader.removeClass('content-header--error').addClass('content-header--error');
        domContentFooter.removeClass('content-footer--error')
            .addClass('content-footer--error')
            .empty()
            .append(
                "<p class='content-footer__text'>¡Error! Revise el campo fecha.</p>"
            );
        return false;
    }


    let timeFormat = ['H:mm'];
    let validateTime = moment(inputTime,  timeFormat, true).isValid();
    if (validateTime === false) {
        domContentHeader.removeClass('content-header--error').addClass('content-header--error');
        domContentFooter.removeClass('content-footer--error')
            .addClass('content-footer--error')
            .empty()
            .append(
                "<p class='content-footer__text'>¡Error! Revise el campo hora.</p>"
            );
        return false;
    }

    return true;
}

/**
 * Send form with AJAX system
 *
 * @param action
 * @param serialize
 * @param domContentHeader
 * @param domContentFooter
 * @param inputPrivacy
 * @param inputMembership
 * @param inputDate
 * @param inputTime
 * @param inputKey
 *
 * @returns {boolean}
 */
function sendForm(action, serialize, domContentHeader, domContentFooter, inputPrivacy, inputMembership, inputDate, inputTime, inputKey) {
    let requestURL = '/' + action + '?timestamp=' + new Date().getTime();
    jQuery.ajax({
        type: 'post',
        url: requestURL,
        data: serialize,
        dataType: 'json',
        cache: false,
        crossDomain: true,
        beforeSend: function (jqXHR) {
            domContentHeader.removeClass('content-header--error');
            domContentFooter.removeClass('content-footer--error')
                .empty()
                .append(
                    "<p class='content-footer__text'>¡Estupendo! La petición se esta procesando =)</p>"
                );
        },
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            let status = data.status;
            let message = data.message;

            if (status === 200) {
                domContentHeader.removeClass('content-header--error')
                    .removeClass('content-header--success')
                    .addClass('content-header--success');
                domContentFooter.removeClass('content-footer--error')
                    .removeClass('content-header--success')
                    .addClass('content-footer--success')
                    .empty()
                    .append("<p class='content-footer__text'>" + message + "</p>");
            } else {
                domContentHeader.removeClass('content-header--error')
                    .removeClass('content-header--success')
                    .addClass('content-header--error');
                domContentFooter.removeClass('content-footer--error')
                    .removeClass('content-header--success')
                    .addClass('content-footer--error')
                    .empty()
                    .append("<p class='content-footer__text'>" + message + "</p>");
            }
            cleanForm();
            return true;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('error', jqXHR, textStatus, errorThrown);
            domContentHeader.removeClass('content-header--error').addClass('content-header--error');
            domContentFooter.removeClass('content-footer--error')
                .addClass('content-footer--error')
                .empty()
                .append(
                    "<p class='content-footer__text'>¡Ops! Ha ocurrido un error, pruebe más tarde.</p>"
                );
        }
    });

    return false;
}

/**
 * Submit form
 */
function submitForm() {
    jQuery('#formAppoitment').submit(function(e) {
        e.preventDefault();

        let domContentHeader = jQuery('.content-header'),
            domContentFooter = jQuery('.content-footer'),
            inputPrivacy = jQuery("#inputPrivacy").prop('checked'),
            inputMembership = this.inputMembership.value,
            inputDate = this.inputDate.value,
            inputTime = this.inputTime.value,
            inputKey = this.inputKey.value;

        let validate = validateForm(
            domContentHeader,
            domContentFooter,
            inputPrivacy,
            inputMembership,
            inputDate,
            inputTime
        );
        if (!validate) {
            return false;
        }

        let action = jQuery(this).attr('action'),
            serialize = jQuery(this).serialize(),
            send = sendForm(
                action,
                serialize,
                domContentHeader,
                domContentFooter,
                inputPrivacy,
                inputMembership,
                inputDate,
                inputTime,
                inputKey
            );

        return false;
    });
}
