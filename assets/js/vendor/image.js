/**
 * Use PixaBay to generate quality images dynamically
 */
function getBackgroundImage() {
    if (isMobile()) {
        return;
    }

    let endpoint = 'https://pixabay.com/api/';
    let apiKey = '1449414-9bfc2006e9a53841a5ef61941';
    let request = endpoint + '?key=' + apiKey + '&image_type=photo&orientation=horizontal' +
        '&order=popular&category=backgrounds&min_width=1600&min_height=1080&per_page=10';

    jQuery.ajax({
        type: 'get',
        url: request,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            let domMain = jQuery('.body');

            if (parseInt(data.totalHits) === 0) {
                domMain.css(
                    "background-image",
                    "url('https://placehold.it/1920x1080&text=OnHealth')"
                );
                return;
            }

            let random = Math.floor(Math.random() * 9);
            let largeImageURL = data.hits[random].largeImageURL;
            domMain.css("background-image", "url('"+ largeImageURL + "')");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('error', jqXHR, textStatus, errorThrown);
        }
    });
}
