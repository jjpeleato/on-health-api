# API Restful: On Health

- https://onhealth-api.alphamega.es/
- https://onhealth-api.alphamega.es/api/

## Project

Built with **API platform** using the following technologies: Symfony 4, Doctrine 2, HTML 5, Bootstrap 4, JavaScript, jQuery, CSS3, SASS & Compass, Lando (Docker), NPM or Yarn and Gulp v.4.

API Platform is a powerful but easy to use full stack framework dedicated to API-driven projects. It contains a PHP library to create fully featured APIs supporting industry-leading standards (JSON-LD, GraphQL, OpenAPI...), provides ambitious JavaScript tooling to consume those APIs in a snap (admin, PWA and mobile apps generators, hypermedia client...).

### Installing dependencies

- You have to install **Lando**: https://docs.devwithlando.io/

If Lando's tools does not work for you, there is another way. You must install the environment manually: XAMP, Composer, Node.JS, NPM or Yarn and Gulp CLI.

For more information visit:

- XAMP: https://www.apachefriends.org/es/index.html
- Composer: https://getcomposer.org/
- Node and NPM: https://nodejs.org/es/
- Yarn: https://yarnpkg.com/es-ES/
- Gulp: https://gulpjs.com/

**Note:** If you work with Windows. To execute the commands, we recommend installing **Cygwin** http://www.cygwin.com/

**Note:** I recommend installing the following IDE for PHP Programming: Visual Studio Code (https://code.visualstudio.com/) or PHPStorm (recommended) (https://www.jetbrains.com/phpstorm/).

### Project skeleton

```
├─ assets/ # Assets front-end directory for compiled to public/assets/*
├─ bin/ # Symfony console
│  ├─ console
├─ config/ # Routes, JWT keys, Symfony bundles and configuration files.
│  ├─ jwt/
│  ├─ packages/
│  ├─ routes/
│  ├─ bootstrap.php
│  ├─ bundles.php
│  ├─ routes.yaml
│  └─ services.yaml
├─ gulp/
│  ├─ task/
│  └─ config.js # Paths and configuration Gulp system.
├─ private/ # Private directory.
│  ├─ config/
│  ├─ sql/ # SQL files for import to minimal or fake data
│  └─ .htaccess
├─ public/ # Public directory (DocumentRoot).
├─ src/ # Source directory.
│  ├─ Controller/
│  ├─ Entity/
│  ├─ Repository/
│  └─ Kernel.php
├─ templates/ # Views directory
├─ .babelrc
├─ .editorconfig
├─ .env.dist # Environment Variables
├─ .gitignore
├─ .jshintignore
├─ .jshintrc
├─ .lando.yml # Lando recipe
├─ .stylelintignore
├─ .stylelintrc
├─ composer.json
├─ deploy.sh.dist # Shell Script for deploy to environments
├─ deploy-exclude-list.txt
├─ gulpfile.babel.js
├─ package.json
├─ phpcs.xml.dist
├─ README.md
├─ script_database.sh # Clear and create fake data or minimal data
└─ script_work.sh # Commands to clean cache and update database
```

### Installing

1. If required. Open the `.lando.yml` and rename the project and proxy name.
2. Open your terminal and browse to the root location of your project.
3. Run `$lando start`.
	- The project has a `.lando.yml` file with all the environment settings.
	- The command starts the installation process when it finishes, you can see all the URLs to access.
4. Copy `.env.dist` to `.env` and edit the credentials. Example:
    - `APP_ENV=dev` Write: dev, test or prod.
    - `APP_DEBUG=true` Write: true or false.
    - `APP_SECRET=0167fb00dc2b09153cae7a8157b93a8b` Write a ramdon secret key of length 32 characters.
    - `APP_KEY=BW8Jf33j3bdRo6u3x7I4` Write a ramdon secret key of length 20 characters.
    - `CORS_ALLOW_ORIGIN=^http?://localhost(:[0-9]+)?$` Allow URL to the CORS system. Put the correct URI.
    - `DATABASE_URL=mysql://lamp:lamp@database:3306/lamp` If you work with lando.
    - `JWT_PASSPHRASE=62429f38cb8270916c538e37e48a75d8` Write a ramdon passphrase key of length 32 characters.
5. Copy `deploy.sh.dist` to `deploy.sh` and edit the credentials according repository and SSH data.
    - `GIT_URI`
    - `SSH_DIRECTORY`
6. Copy `phpcs.xml.dist` to `phpcs.xml` and not edit.
6. Copy `public/humans.txt.dist` to `public/humans.txt` and edit.
7. If required. Run: `$lando composer install`.
8. If required. Run: `$lando npm install --save-dev`.
9. Generate the SSH keys:
    - `$lando ssh` then:
        - `$mkdir -p config/jwt # For Symfony3+, no need of the -p option`
        - `$openssl genrsa -out config/jwt/private.pem -aes256 4096`
        - `$openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem`
10. Run `$ssh script_database.sh`
11. Run `$ssh script_local.sh` or `$ssh script_prod.sh`
12. End. Happy developing.

### Developing

- Open your terminal and browse to the root location of your project.
- If required. Run: `$lando composer install` then `$lando console [action]`.
    - `$lando console` List all commands.
    - `$lando console doctrine:schema:drop --force` Drops the configured database.
    - `$lando console doctrine:schema:create`  Creates the configured database.
    - `$lando console doctrine:schema:update --force` Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata.
    - `$lando console cache:clear` Clears the cache. Develop environment.
    - `$lando console cache:clear --env test` Clears the cache. Test environment.
    - `$lando console cache:clear --env prod` Clears the cache. Production environment.
- If you work with PHP CodeSniffer then `$lando composer [action]`.
	- `$lando composer cs` Runs the phpcs.
	- `$lando composer cs:fix` Runs the phpcbf.
- If required. Run: `$lando npm install --save-dev` or `$lando yarn install --dev` then: `$lando gulp [action]`
- To work with and compile your Sass and JS files on the fly start: `$lando gulp`
- Gulp actions commands list:
    - `$lando gulp clean` Delete all files.
    - `$lando gulp css` Compile SASS to CSS and validate SASS according Stylelint (https://stylelint.io/). Not concat.
    - `$lando gulp cssAssets` Copy CSS assets to public directory.
    - `$lando gulp cssWithConcat` Concat and compile SASS to CSS and validate SASS according Stylelint (https://stylelint.io/).
    - `$lando gulp fontAssets` Copy fonts assets to public directory.
    - `$lando gulp images` Copy and minify PNG, JPEG, GIF and SVG images with imagemin.
    - `$lando gulp imagesAssets` Copy and minify PNG, JPEG, GIF and SVG assets images with imagemin.
    - `$lando gulp js` Validate the code with JSHint. Minify the JS files.
    - `$lando gulp jsAssets` Copy JS assets to public directory.
    - `$lando gulp jsWithConcat` Validate the code with Jshint. Concat and minify the JS files.
    - `$lando gulp validateJs` Validate JS with JSHint (https://jshint.com/).
    - `$lando gulp validateScss` Validate SCSS according Stylint (https://stylelint.io/).
    - `$lando gulp watch` Compile SASS to CSS and concat and minify JS files in real-time.
- NPM actions commands list:
    - `$lando npm run gulp:dev` Compile for development environment
    - `$lando npm run gulp:prod` Compile for production environment

### Deploy to environments

The present project uses a bash file called `deploy.sh`. Execute all commands to deploy in any environment. You only have to modify the git repository and server data.
1. Run `$lando ssh` then: `$sh deploy.sh`.
2. Open your terminal, you connect to PROD environment with SSH and browse to the root location of your project.
3. Run `$sh script_prod.sh`
4. If required. Run `$sh script_database.sh`

### Technologies and tools

The present project uses several technologies and tools for the automation and development process. For more information and learning visit the following links.

1. API Platform: https://api-platform.com/
2. Symfony 4: https://symfony.com/
3. Doctrine 2: https://www.doctrine-project.org/
4. Guzzle: http://docs.guzzlephp.org
5. UUID: https://github.com/ramsey/uuid
6. LexikJWTAuthenticationBundle: https://github.com/lexik/LexikJWTAuthenticationBundle
7. Git: https://git-scm.com/
8. Lando: https://docs.devwithlando.io/
9. Deployer: https://deployer.org/
10. Composer: https://getcomposer.org/
11. NPM: https://www.npmjs.com/
12. Yarn: https://yarnpkg.com/
13. Sass: https://sass-lang.com/
14. Gulp: https://gulpjs.com/
15. Babel: https://babeljs.io/
16. Bootstrap: https://getbootstrap.com/
17. EditorConfig: https://editorconfig.org/
18. PHP_CodeSniffer: https://github.com/squizlabs/PHP_CodeSniffer
19. Stylelint: https://stylelint.io/
20. Jshint: https://jshint.com/
21. Human.txt: http://humanstxt.org/
22. Moment.js: http://momentjs.com/
23. Datepicker: https://github.com/qodesmith/datepicker/
24. Timepicker: https://timepicker.co/options/

**Note:** Thanks a lot of developers that to work on this projects.

### Clarifications

1. It is very important that if you deploy the project to publish. The **DocumentRoot** on the VirtualHost has to point to the **public/** directory.
2. It is very important that if you deploy the project to publish with **Deployer**. The **DocumentRoot** on the VirtualHost has to point to the **current/public/** directory.

### Others clarifications

1. It is possible that on Mac OS the Gulp tasks do not run the correct form. In this case install NodeJS, NPM and Gulp-cli in your OS and execute the tasks outside the Docker containers.

## Finally

**Remove all themes or plugins that you don't use.**

More information on the following commits. If required.

Grettings **@jjpeleato**.
