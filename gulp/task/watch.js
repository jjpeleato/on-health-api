'use strict';

/*
 * Import require functions
 */
import gulp from 'gulp';

import config from '../config';
import {css, cssWithConcat} from './styles';
import {js, jsWithConcat} from './scripts';
import { validateScss } from './styles';
import { validateJs } from './scripts';

/* Tasks */
function watch()
{
	gulp.watch(config.paths.sassAssets.src, { ignoreInitial: false }, gulp.series(validateScss, cssWithConcat));
	gulp.watch(config.paths.jsAssets.src, { ignoreInitial: false }, gulp.series(validateJs, jsWithConcat));
}

exports.watch = watch;
